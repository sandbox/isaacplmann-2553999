var angularjsBlockExample = angular.module('angularjsBlockExample', ['ngResource'])

  // // Factory for the ngResource service.
  // .factory('Node', function($resource) {
  //   return $resource(Drupal.settings.basePath + 'rest/entity_node/:param', {}, {
  //     'search' : {method : 'GET', isArray : true}
  //   });
  // })
  // .factory('FieldCollection', function($resource) {
  //   return $resource(Drupal.settings.basePath + 'rest/entity_field_collection_item/:param', {}, {
  //     'search' : {method : 'GET', isArray : true}
  //   });
  // })
  // .controller('angularjsBlockController', ['$scope', 'Node', 'FieldCollection', function($scope, Node, FieldCollection) {
  .controller('angularjsBlockController', ['$scope', function($scope) {
    $scope.items = [
      'Item1','Item2','Item3'
    ];
  }]);
jQuery(document).ready(function() {
  angular.bootstrap(document.getElementById('angularjsBlock'), ['angularjsBlockExample']);
});

